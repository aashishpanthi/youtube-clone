import React,{ createContext } from 'react'

export const VideosContext = createContext()

export const StateProvider = ({ videos, children }) =>{
	return(
	<VideosContext.Provider value={videos}>
		{children}
	</VideosContext.Provider>
	)}