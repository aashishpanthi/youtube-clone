import React from 'react'
import { Sidebar, RecommendedVideos } from '../components'

const HomePage = () =>{
    return (
        <>
            <Sidebar />
            <RecommendedVideos />
        </>
    )
}

export default HomePage