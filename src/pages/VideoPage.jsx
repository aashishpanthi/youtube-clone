import React,{ useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
// import { Grid } from '@material-ui/core'

const VideoPage = () =>{

    const [url, setUrl ] = useState('')
    const location = useLocation();

    useEffect(()=>{
        setUrl(location.pathname.split('watch/')[1])
    })

    return (
        <div className="VideoPage">
            <div className="VideoPage__videoPart">
                {/* <videoPreview /> */}
                {/* videotitle */}
                {/* videoDesc */}
                {url ? (<iframe src={`https://www.youtube.com/embed/${url}`} width="600" height="450" style={{border:'none'}}>
                </iframe>):(
                    <h1>Loading...</h1>
                )}
            </div>

            <div className="VideoPage__videoRecommended">
                {/* Recommended */}
                    {/* <Video /> */}
                    {/* <Video /> */}
                    {/* <Video /> */}
                    {/* <Video /> */}
                    {/* <Video /> */}
            </div>
        </div>
    )
}

export default VideoPage