import React,{ useState, useEffect } from 'react'
// import { response } from './responses'
import youtube from '../api/youtube'
import { useLocation, Link } from 'react-router-dom'
import { Sidebar,/* ChannelRow,*/ VideoRow } from '../components'
import TuneOutlinedIcon from '@material-ui/icons/TuneOutlined'
import '../ess/css/SearchPage.css'

function SearchPage() {
	const [videos, setVideos] = useState([])
	let location = useLocation();
	
	const fetchVideos = async (query) =>{
		const response = await youtube.get('search',{
			params :{
				part:'snippet',
				maxResults: '10',
				key:'AIzaSyC1eAAJQagcg3eP2X52j5OSMrgmOsfKmsc',
				q: query,
			}
		})
		
		// console.log(response)
		setVideos(response.data.items)
	}
	
	useEffect(() => {
		fetchVideos(location.pathname.split('search/')[1])
		// console.log(response)
		// setVideos(response.items)
	}, [])
		

	return (
		<>
			<Sidebar />
			<div className='searchPage'>
				<div className='searchPage__filter'>
					<TuneOutlinedIcon />
					<h3>FILTER</h3>
				</div>
				<div className='searchPage__result'>
					{/* <ChannelRow 
						image='https://yt3.ggpht.com/a/AATXAJx6xTINDk0rLtmErlBlX6G335P6fKmnT_y4B7TKMw=s176-c-k-c0x00ffffff-no-rj-mo'
						name='Aashish Panthi'
						verified
						subs='32'
						noOfVideos='2'
						description='Learn more in less time-- Get one minute explanation on the topics. Mainly topics are related with technologies such as: Web development, Programming'
					/> */}

				{videos[0] ? (
					videos.map((video,index) => (
						<Link to={`/watch/${video.id.videoId}`}>
							<VideoRow 
								key={index}
								title={video.snippet.title}
								channelName={video.snippet.channelTitle}
								views='942k views'
								description={video.snippet.description}
								timestamp='2 years ago'
								channelPic='https://yt3.ggpht.com/a-/AOh14GgwaiCp63JOClbTXswJ4u8x9IXpD_qDn3tt3g=s68-c-k-c0x00ffffff-no-rj-mo'
								thumbnailPic={video.snippet.thumbnails.high.url}
							/>
						</Link>
					))
				): (<h1>Loading...</h1>)}
				</div>
			</div>
		</>
	)
}

export default SearchPage