import React from 'react';
import { Header } from './components'
import { SearchPage, HomePage, VideoPage, ErrorPage } from './pages'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import './App.css';

function App() {
  return (
    <div className="app">
      <BrowserRouter>
      	<Header />
		<Switch>
			<Route path='/watch/:video'>
				<VideoPage />	
			</Route>
			<div className='app_body'>
				<Switch>
					<Route path='/search/:searchQuery'>
						<SearchPage />	
					</Route>
					<Route path='/'>
						<HomePage />
					</Route>
				</Switch>
			</div>
			<Route component={ErrorPage} />
		</Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
