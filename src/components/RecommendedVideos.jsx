import React from 'react'
import Video from './Video'
import '../ess/css/RecommendedVideos.css'


function RecommendedVideos() {
	return (
		<div className='RecommendedVideos'>
			<h2>Recommended</h2>
			<div className='RecommendedVideos__videos'>
				<Video 
					title='Database Design Course || Learn how to design'
					channelName='freeCodeCamp.org'
					views='942k views'
					timestamp='2 years ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgwaiCp63JOClbTXswJ4u8x9IXpD_qDn3tt3g=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/HXV3zeQKqGY/hq720.jpg?sqp=-oaymwEZCOgCEMoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAsFQnVfDkctPc1BxGyFIi_rhzq_A'
				/>
				<Video 
					title='Will Flutter kill React Native'
					channelName='Ben Awad'
					views='39k views'
					timestamp='1 year ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgSTCjrwSQFEd8ZlZ4wtck8SWB-3KSyZN6DQg=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/uw4lh4zsTs0/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAu4zbSgdSbmjC6YvE3ZnEML-xjeQ'
				/>
				<Video 
					title='Database Design Course || Learn how to design'
					channelName='freeCodeCamp.org'
					views='942k views'
					timestamp='2 years ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgwaiCp63JOClbTXswJ4u8x9IXpD_qDn3tt3g=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/HXV3zeQKqGY/hq720.jpg?sqp=-oaymwEZCOgCEMoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAsFQnVfDkctPc1BxGyFIi_rhzq_A'
				/>
				<Video 
					title='Will Flutter kill React Native'
					channelName='Ben Awad'
					views='39k views'
					timestamp='1 year ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgSTCjrwSQFEd8ZlZ4wtck8SWB-3KSyZN6DQg=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/uw4lh4zsTs0/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAu4zbSgdSbmjC6YvE3ZnEML-xjeQ'
				/>
				<Video 
					title='Database Design Course || Learn how to design'
					channelName='freeCodeCamp.org'
					views='942k views'
					timestamp='2 years ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgwaiCp63JOClbTXswJ4u8x9IXpD_qDn3tt3g=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/HXV3zeQKqGY/hq720.jpg?sqp=-oaymwEZCOgCEMoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAsFQnVfDkctPc1BxGyFIi_rhzq_A'
				/>
				<Video 
					title='Will Flutter kill React Native'
					channelName='Ben Awad'
					views='39k views'
					timestamp='1 year ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgSTCjrwSQFEd8ZlZ4wtck8SWB-3KSyZN6DQg=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/uw4lh4zsTs0/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAu4zbSgdSbmjC6YvE3ZnEML-xjeQ'
				/>
				<Video 
					title='Database Design Course || Learn how to design'
					channelName='freeCodeCamp.org'
					views='942k views'
					timestamp='2 years ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgwaiCp63JOClbTXswJ4u8x9IXpD_qDn3tt3g=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/HXV3zeQKqGY/hq720.jpg?sqp=-oaymwEZCOgCEMoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAsFQnVfDkctPc1BxGyFIi_rhzq_A'
				/>
				<Video 
					title='Will Flutter kill React Native'
					channelName='Ben Awad'
					views='39k views'
					timestamp='1 year ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgSTCjrwSQFEd8ZlZ4wtck8SWB-3KSyZN6DQg=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/uw4lh4zsTs0/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAu4zbSgdSbmjC6YvE3ZnEML-xjeQ'
				/>
				<Video 
					title='Database Design Course || Learn how to design'
					channelName='freeCodeCamp.org'
					views='942k views'
					timestamp='2 years ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgwaiCp63JOClbTXswJ4u8x9IXpD_qDn3tt3g=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/HXV3zeQKqGY/hq720.jpg?sqp=-oaymwEZCOgCEMoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAsFQnVfDkctPc1BxGyFIi_rhzq_A'
				/>
				<Video 
					title='Will Flutter kill React Native'
					channelName='Ben Awad'
					views='39k views'
					timestamp='1 year ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgSTCjrwSQFEd8ZlZ4wtck8SWB-3KSyZN6DQg=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/uw4lh4zsTs0/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAu4zbSgdSbmjC6YvE3ZnEML-xjeQ'
				/>
				<Video 
					title='Database Design Course || Learn how to design'
					channelName='freeCodeCamp.org'
					views='942k views'
					timestamp='2 years ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgwaiCp63JOClbTXswJ4u8x9IXpD_qDn3tt3g=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/HXV3zeQKqGY/hq720.jpg?sqp=-oaymwEZCOgCEMoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAsFQnVfDkctPc1BxGyFIi_rhzq_A'
				/>
				<Video 
					title='Will Flutter kill React Native'
					channelName='Ben Awad'
					views='39k views'
					timestamp='1 year ago'
					channelPic='https://yt3.ggpht.com/a-/AOh14GgSTCjrwSQFEd8ZlZ4wtck8SWB-3KSyZN6DQg=s68-c-k-c0x00ffffff-no-rj-mo'
					thumbnailPic='https://i.ytimg.com/vi/uw4lh4zsTs0/hq720.jpg?sqp=-oaymwEZCNAFEJQDSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAu4zbSgdSbmjC6YvE3ZnEML-xjeQ'
				/>
			</div>
		</div>
	)
}

export default RecommendedVideos