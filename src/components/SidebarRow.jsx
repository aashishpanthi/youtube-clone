import React from 'react'

function SidebarRow({selected, Icon ,title}) {
	return (
		<div className={`sidebarRow ${selected && 'selected'}`}>
			<Icon className='sidebarRow__icon'/>
			<span>{title}</span>
		</div>
	)
}

export default SidebarRow