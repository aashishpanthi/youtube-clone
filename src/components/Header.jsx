import React,{ useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import MenuIcon from '@material-ui/icons/Menu'
import VideoCallIcon from '@material-ui/icons/VideoCall'
import NotificationIcon from '@material-ui/icons/Notifications'
import YouTubeIcon from '@material-ui/icons/YouTube'
import SearchIcon from '@material-ui/icons/Search'
import AppsIcon from '@material-ui/icons/Apps'
import {Avatar} from '@material-ui/core'
import '../ess/css/Header.css'

function Header() {

	const [search, setSearch] = useState('')
	const history = useHistory()
	const submitSearch = e =>{
		e.preventDefault()
		history.push(`/search/${search}`)
	}

	return (
		<div className='header'>
			<div className='header__left'>	
				<MenuIcon />
				<Link to='/' className='header__youtubeLogoLink'>
					<div className='header__youtubeLogo'>
						<YouTubeIcon className='header__youtubeIcon' />
						<span>YouTube</span>
					</div>
				</Link>
			</div>
			<form className='header__middle'>
				<input 
					type="text"
					placeholder=' Search'
					value={search}
					onChange={e => setSearch(e.target.value)}
				/>
				{/* <Link to={`/search/${search}`}> */}
				<button type='submit' onClick={submitSearch} className='header__searchButton'>
					<SearchIcon className='header__searchIcon' />
				</button>
				{/* </Link> */}
			</form>
			<div className='header__right'>
				<VideoCallIcon className='header__rightIcon' />
				<AppsIcon className='header__rightIcon' />
				<NotificationIcon className='header__rightIcon' />
				<Avatar />
			</div>
		</div>
	)
}

export default Header