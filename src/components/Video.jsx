import React from 'react'
import {Avatar} from '@material-ui/core'

function Video({title,channelName,views,timestamp,channelPic,thumbnailPic}){
	return (
		<div className='video'>
			<img src={thumbnailPic} alt="thumbnail" className='video__thumbnail' />
			<div className='video__info'>
				<Avatar src={channelPic} alt='channel pic' />
				<div className='video__text'>
					<h4> {title} </h4>
					<p> {channelName} </p>
					<p>
						{views} {timestamp}
					</p>
				</div>
			</div>
		</div>
	)
}

export default Video