import React from 'react'
import { Avatar } from '@material-ui/core'
import VerifiedIcon from '@material-ui/icons/CheckCircleOutlineOutlined'

function ChannelRow({image,name,verified,subs,noOfVideos,description}) {
	return (
		<div className='channelRow'>
			<Avatar 
				className="channelRow__avatar"
				src={image} 
				alt={name}
			/>

			<div className='channelRow__text'>
				<h4>{name} {verified && <VerifiedIcon />}</h4>
				<p>
					{subs} subscribers {noOfVideos} videos
				</p>
				<p>{description}</p>
			</div>

		</div>
	)
}

export default ChannelRow