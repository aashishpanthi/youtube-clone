import React from 'react'

function VideoRow({title,description,channelName,subs,views,timestamp,channelPic,thumbnailPic}) {
	return (
		<div className='videoRow'>
			<img src={thumbnailPic} alt="thumbnail" className='videoRow__thumbnail' />
			<div className='videoRow__text'>
				<h4> {title} </h4>
				<p className='videoRow__headline'> 
					{views} {timestamp} 
				</p>
				<p className='videoRow__headline' style={{marginTop:'10px'}}>
					<span>
						<span>{channelName} </span> 
					</span> 
				</p>
				<p className='videoRow__desc'>{description}</p>
			</div>
		</div>
	)
}

export default VideoRow