import React from 'react'
import HomeIcon from '@material-ui/icons/Home'
import SubscriptionsIcon from '@material-ui/icons/Subscriptions'
import WhatshotIcon from '@material-ui/icons/Whatshot'
import SidebarRow from './SidebarRow'
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary'
import HistoryIcon from '@material-ui/icons/History'
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo'
import WatchLaterIcon from '@material-ui/icons/WatchLater'
import PlaylistPlayIcon from '@material-ui/icons/PlaylistPlay'
import ThumbUpRoundedIcon from '@material-ui/icons/ThumbUpRounded'
import ExpandMoreOutlinedIcon from '@material-ui/icons/ExpandMoreOutlined'
import '../ess/css/Sidebar.css'

function Sidebar() {
	return (
		<div className='sidebar'>
			<div className='sidebar__topItems'>
				<SidebarRow selected title='Home' Icon={HomeIcon} />
				<SidebarRow title='Trending' Icon={WhatshotIcon} />
				<SidebarRow title='Subscriptions' Icon={SubscriptionsIcon} />
			</div>
			<div className='sidebar__bottomItems'>
				<SidebarRow title='Library' Icon={VideoLibraryIcon} />
				<SidebarRow title='History' Icon={HistoryIcon} />
				<SidebarRow title='Your videos' Icon={OndemandVideoIcon} />
				<SidebarRow title='Watch Later' Icon={WatchLaterIcon} />
				<SidebarRow title='Playlists' Icon={PlaylistPlayIcon} />
				<SidebarRow title='Liked Videos' Icon={ThumbUpRoundedIcon} />
				<SidebarRow title='See More' Icon={ExpandMoreOutlinedIcon} />
			</div>
		</div>
	)
}

export default Sidebar